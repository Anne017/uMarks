#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Ernesst <slash.tux@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import datetime
import json
import traceback
import os, sys, inspect
from collections import OrderedDict
JSONFILE = "/home/phablet/.local/share/umarks.ernesst/bookmark.json"

def sort_JSON(title):

 with open(JSONFILE) as input:
  data_loaded = json.load(input)

  #Reverse order of writing the JSON
  data_shorted = OrderedDict(data_loaded)
  data_shorted['data'] = sorted(data_loaded['data'], key=lambda x : x[title])

 with open(JSONFILE, 'w') as json_file:
  json.dump(data_shorted, json_file)
  json_file.close()
